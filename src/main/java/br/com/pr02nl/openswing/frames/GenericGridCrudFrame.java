/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pr02nl.openswing.frames;

import br.com.pr02nl.annotations.ColumnType;
import br.com.pr02nl.annotations.TipoColuna;
import br.com.pr02nl.model.BaseModel;
import br.com.pr02nl.openswing.views.GridGenericoAbstract;
import br.gov.frameworkdemoiselle.util.Reflections;
import java.lang.reflect.Field;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Entity;
import org.openswing.swing.client.DeleteButton;
import org.openswing.swing.client.EditButton;
import org.openswing.swing.client.ExportButton;
import org.openswing.swing.client.FilterButton;
import org.openswing.swing.client.GenericButton;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.client.InsertButton;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.client.SaveButton;
import org.openswing.swing.items.client.ItemsDataLocator;
import org.openswing.swing.lookup.client.LookupController;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.table.columns.client.CodLookupColumn;
import org.openswing.swing.table.columns.client.Column;
import org.openswing.swing.table.columns.client.ComboColumn;
import org.openswing.swing.table.columns.client.ComboVOColumn;
import org.openswing.swing.table.columns.client.CurrencyColumn;
import org.openswing.swing.table.columns.client.DateColumn;
import org.openswing.swing.table.columns.client.DateTimeColumn;
import org.openswing.swing.table.columns.client.DecimalColumn;
import org.openswing.swing.table.columns.client.FileColumn;
import org.openswing.swing.table.columns.client.IntegerColumn;
import org.openswing.swing.table.columns.client.TextColumn;

/**
 *
 * @author Paulo Roberto
 * @param <T>
 */
public abstract class GenericGridCrudFrame<T extends BaseModel> extends InternalFrame {

    private Class<T> modelClass;
    private org.openswing.swing.client.DeleteButton deleteButton;
    private org.openswing.swing.client.EditButton editButton;
    private org.openswing.swing.client.ExportButton exportButton;
    private org.openswing.swing.client.FilterButton filterButton;
    private org.openswing.swing.client.GridControl gridControl;
    private org.openswing.swing.client.InsertButton insertButton;
    private org.openswing.swing.client.ReloadButton reloadButton;
    private org.openswing.swing.client.SaveButton saveButton;
    private org.openswing.swing.client.NavigatorBar navigatorBar;
    private javax.swing.JPanel jPanel1;
    private final Map<String, Column> mapColumns = new HashMap<String, Column>();

    public GenericGridCrudFrame() {
        initComponents();
        setSize(750, 300);
    }

    public void init() {
        getGridControl().setController(getView());
        getGridControl().setGridDataLocator(getView());
    }

    protected Class<T> getModelClass() {
        if (modelClass == null) {
            modelClass = Reflections.getGenericTypeArgument(this.getClass(), 0);
        }
        return modelClass;
    }

    public GridControl getGridControl() {
        return gridControl;
    }

    private void initComponents() {
        gridControl = new org.openswing.swing.client.GridControl();
        jPanel1 = new javax.swing.JPanel();
        insertButton = new org.openswing.swing.client.InsertButton();
        editButton = new org.openswing.swing.client.EditButton();
        deleteButton = new org.openswing.swing.client.DeleteButton();
        saveButton = new org.openswing.swing.client.SaveButton();
        reloadButton = new org.openswing.swing.client.ReloadButton();
        filterButton = new org.openswing.swing.client.FilterButton();
        exportButton = new org.openswing.swing.client.ExportButton();
        navigatorBar = new org.openswing.swing.client.NavigatorBar();
        gridControl.setAnchorLastColumn(true);
        gridControl.setDeleteButton(deleteButton);
        gridControl.setEditButton(editButton);
        gridControl.setExportButton(exportButton);
        gridControl.setFilterButton(filterButton);
        gridControl.setInsertButton(insertButton);
        gridControl.setReloadButton(reloadButton);
        gridControl.setSaveButton(saveButton);
        gridControl.setNavBar(navigatorBar);
        gridControl.setValueObjectClassName(getModelClass().getName());
        gridControl.getColumnContainer().setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));
        jPanel1.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));
        getContentPane().add(gridControl, java.awt.BorderLayout.CENTER);
        getContentPane().add(jPanel1, java.awt.BorderLayout.PAGE_START);
        jPanel1.add(insertButton);
        jPanel1.add(editButton);
        jPanel1.add(deleteButton);
        jPanel1.add(saveButton);
        jPanel1.add(reloadButton);
        jPanel1.add(filterButton);
        jPanel1.add(exportButton);
        jPanel1.add(navigatorBar);

        createColumn(getModelClass(), "");
    }

    public Column addColumn(String columnName, int index) throws NoSuchFieldException {
        if (columnName == null || "".equals(columnName)) {
            throw new RuntimeException("Não existe propriedade");
        }
        String[] split = columnName.split("\\.");
        Field declaredField;
        if (split.length > 0) {
            try {
                declaredField = getModelClass().getDeclaredField(split[0]);
            } catch (NoSuchFieldException ex) {
                declaredField = getModelClass().getSuperclass().getDeclaredField(split[0]);
            }
            for (int i = 1; i < split.length; i++) {
                try {
                    declaredField = declaredField.getType().getDeclaredField(split[i]);
                } catch (NoSuchFieldException ex) {
                    declaredField = declaredField.getType().getSuperclass().getDeclaredField(split[i]);
                }
            }
        } else {
            declaredField = getModelClass().getDeclaredField(columnName);
        }
        return addColumn(declaredField, columnName.replace(declaredField.getName(), ""), index);
    }

    public Column addColumn(String columnName) throws NoSuchFieldException {
        return addColumn(columnName, -1);
    }

    private Column addColumn(Field field, String upName, int index) {
        Column column = null;
        if (field.isAnnotationPresent(ColumnType.class)) {
            ColumnType annotation = field.getAnnotation(ColumnType.class);
            TipoColuna value = annotation.value();
            switch (value) {
                case CURRENCY:
                    column = new CurrencyColumn();
                    ((CurrencyColumn) column).setMaxValue(Double.MAX_VALUE);
                    ((CurrencyColumn) column).setMinValue(Double.MIN_VALUE);
                    ((CurrencyColumn) column).setDecimals(2);
                    ((CurrencyColumn) column).setGrouping(true);
                    break;
                case FILE:
                    column = new FileColumn();
                    break;
                case NONE:
                    column = null;
                    break;
                case DATETIME:
                    column = new DateTimeColumn();
                    break;
                default:
                    throw new AssertionError();
            }
        } else {
            if (field.getType().equals(String.class)) {
                column = new TextColumn();
            } else if (field.getType().equals(Integer.class) || field.getType().equals(int.class)
                    || field.getType().equals(long.class) || field.getType().equals(Long.class)) {
                column = new IntegerColumn();
            } else if (field.getType().equals(Double.class) || field.getType().equals(double.class)
                    || field.getType().equals(Float.class) || field.getType().equals(float.class)) {
                column = new DecimalColumn();
            } else if (field.getType().equals(Boolean.class) || field.getType().equals(boolean.class)) {
                column = new ComboColumn();
                ((ComboColumn) column).setDomainId("BOOLEAN_OPTIONS"); //Precisa criar o Dominio
            } else if (field.getType().equals(Date.class)) {
                column = new DateColumn();
            } else if (field.getType().isEnum()) {
                column = new ComboColumn();
                ((ComboColumn) column).setDomainId(field.getType().getSimpleName());
            } else if (field.getType().isAnnotationPresent(Entity.class)) {
//                createColumn(field.getType(), upName + field.getName() + ".");
            }
        }
        if (column != null) {
            column.setColumnName(upName + field.getName());
            column.setEditableOnEdit(true);
            column.setEditableOnInsert(true);
            column.setColumnRequired(false);
            column.setColumnFilterable(true);
            column.setColumnSortable(true);
            column.setMaxWidth(1000);
            mapColumns.put(column.getColumnName(), column);
            gridControl.getColumnContainer().add(column, index);
        }
        return column;
    }

    private Column addColumn(Field field, String upName) {
        return addColumn(field, upName, -1);
    }

    public Column addColumn(String alimentadornome, ItemsDataLocator controller, Class valueObjectType, int index) {
        ComboVOColumn column = new ComboVOColumn();
        column.setColumnDuplicable(true);
        column.setColumnFilterable(true);
        column.setColumnName(alimentadornome);
        column.setColumnSortable(true);
        column.setEditableOnEdit(true);
        column.setEditableOnInsert(true);
        column.setColumnRequired(false);
        column.setComboDataLocator(controller);
        column.setPreferredWidth(60);
        column.setComboValueObjectClassName(valueObjectType.getName());
        column.setAllColumnVisible(true);
        column.setVisibleColumn("id", false);
        column.addCombo2ParentLink(alimentadornome);
        column.setForeignKeyAttributeName("id");
        mapColumns.put(column.getColumnName(), column);
        gridControl.getColumnContainer().add(column, index);
        return column;
    }

    public Column addColumn(String alimentadornome, ItemsDataLocator controller, Class valueObjectType) {
        return addColumn(alimentadornome, controller, valueObjectType, -1);
    }

    public Column addColumn(String alimentadornome, LookupController controller, int index) {
        CodLookupColumn column = new CodLookupColumn();
        column.setColumnDuplicable(true);
        column.setColumnFilterable(true);
        column.setColumnName(alimentadornome);
        column.setColumnSortable(true);
        column.setEditableOnEdit(true);
        column.setEditableOnInsert(true);
        column.setColumnRequired(false);
        column.setLookupController(controller);
        column.setPreferredWidth(60);
        mapColumns.put(column.getColumnName(), column);
        gridControl.getColumnContainer().add(column, index);
        return column;
    }

    public Column addColumn(String alimentadornome, LookupController controller) {
        return addColumn(alimentadornome, controller, -1);
    }

    private void createColumn(Class clazz, String upName) {
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            addColumn(field, upName);
        }
    }

    public void addButton(GenericButton button) {
        jPanel1.add(button);
    }

    public Map<String, Column> getMapColumns() {
        return mapColumns;
    }

    public DeleteButton getDeleteButton() {
        return deleteButton;
    }

    public EditButton getEditButton() {
        return editButton;
    }

    public ExportButton getExportButton() {
        return exportButton;
    }

    public FilterButton getFilterButton() {
        return filterButton;
    }

    public InsertButton getInsertButton() {
        return insertButton;
    }

    public ReloadButton getReloadButton() {
        return reloadButton;
    }

    public SaveButton getSaveButton() {
        return saveButton;
    }

    public abstract GridGenericoAbstract getView();
}
